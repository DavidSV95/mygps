package VistasGPS;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.Component;

import ModeloGPS.GPS;
import ModeloGPS.ModelData;
import ModeloGPS.PuntoMapa;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;

/**
 *
 * @author bj0661
 */
public class ViewData extends javax.swing.JFrame {
	private GPS miGps;
	/**
	 * Creates new form Principal
	 */
	public ViewData() {
		
		initComponents();
		setContentPaneFromPane(this, new PantallaInicial());
		this.setTitle("myGPS");
		
	}
	public ViewData(GPS modelo) {
		miGps = modelo;
		initComponents();
		setContentPaneFromPane(this, new PantallaInicial(miGps));
		this.setTitle("myGPS");
		
	}
	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setLocationByPlatform(true);
        setResizable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

	/**
	 * @param args the command line arguments
	 */

	public static void setContentPaneFromPane(JPanel fromPanel, JPanel targetPanel) {
		JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(fromPanel);
		setContentPaneFromPane(frame, targetPanel);
		
	}
	public static void setContentPaneFromPane(JPanel fromPanel, JLayeredPane targetPanel) {
		JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(fromPanel);
		setContentPaneFromPane(frame, targetPanel);
		
	}

	public static void setContentPaneFromPane(JFrame frame, JPanel targetPanel) {
		frame.setContentPane(targetPanel);
		frame.setPreferredSize(targetPanel.getPreferredSize());
		if (targetPanel.getName() != null && !targetPanel.getName().equals("")) {
			frame.setTitle("Aplicación GPS - " + targetPanel.getName());
		}
		frame.pack();
		frame.repaint();
		
	}
	public static void setContentPaneFromPane(JFrame frame, JLayeredPane targetPanel) {
		frame.setContentPane(targetPanel);
		frame.setPreferredSize(targetPanel.getPreferredSize());
		if (targetPanel.getName() != null && !targetPanel.getName().equals("")) {
			frame.setTitle("Aplicación GPS - " + targetPanel.getName());
		}
		frame.setSize(800, 800);
		frame.repaint();
		
		
	}
	public void pintarMedida(String mensaje) {
		JScrollPane scrollPane;
		JViewport viewPort;
		JTextArea textArea;
		scrollPane = (JScrollPane) this.getContentPane().getComponent(1);
		viewPort = (JViewport) scrollPane.getComponent(0);
		textArea = (JTextArea) viewPort.getView();
		 
		
		textArea.append(mensaje);
	}
	public void pintarPunto(PuntoMapa puntoMapa) {
		this.getContentPane().repaint();
		
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
