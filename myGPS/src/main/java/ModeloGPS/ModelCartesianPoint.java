package ModeloGPS;

import javax.swing.JOptionPane;



public class ModelCartesianPoint {
	private static final double cuadradoExcentricidad = 0.00669437999013;
	private static final double radioEcuatorial = 6378137.0;
	private static final double husoEspaña = 30.0;
	
	private ModelData modelData;
	private double longitud;
	private double latitud;
	private double utmEatsing;
	private double utmNorting;
	
	public double getUtmEatsing() {
		return utmEatsing;
	}

	public void setUtmEatsing(double utmEatsing) {
		this.utmEatsing = utmEatsing;
	}

	public double getUtmNorting() {
		return utmNorting;
	}

	public void setUtmNorting(double utmNorting) {
		this.utmNorting = utmNorting;
	}

	public ModelCartesianPoint(ModelData data) {
		this.modelData = data;
		if ("W".equals(modelData.cardinalidadLongitud)){
			this.setLongitud((-1.0) * modelData.getLongitud());
		}
		else {
			this.setLongitud(modelData.getLongitud());
		}
		
		if ("S".equals(modelData.cardinalidadLatitud)){
			this.setLatitud((-1.0) * modelData.getLatitud());
		}
		else {
			this.setLatitud(modelData.getLatitud());
		}
		this.calculateCoord();
		
	}

	private void calculateCoord() {
		
		double longRadians = this.getLongitud() * Math.PI /180.0;
		double latRadians = this.getLatitud() * Math.PI /180.0;
		//TODO no se que que es lambda0 "longitud ..."
		double lambdaCero = (husoEspaña*6.0-183.0)*Math.PI/180.0;
		double excentricidad = cuadradoExcentricidad / (1.0-cuadradoExcentricidad);
		double N = radioEcuatorial / (Math.sqrt(1-cuadradoExcentricidad*Math.pow(Math.sin(latRadians), 2.0)));
		double T = Math.pow(Math.tan(latRadians), 2.0);
		double C = excentricidad * Math.pow(Math.cos(latRadians), 2.0);
		double A = Math.cos(latRadians)*(longRadians - lambdaCero);
		double M = radioEcuatorial
				* ((1.0 - cuadradoExcentricidad / 4.0 - 3.0
						* Math.pow(cuadradoExcentricidad, 2.0) / 64.0 - 5.0 * Math.pow(
						cuadradoExcentricidad, 3.0) / 256.0)
						* latRadians
						- (3.0 * cuadradoExcentricidad / 8.0 + 3.0
								* Math.pow(cuadradoExcentricidad, 2.0) / 32.0 + 45.0 * Math
								.pow(cuadradoExcentricidad, 3.0) / 1024.0)
						* Math.sin(2.0 * latRadians)
						+ (15.0 * Math.pow(cuadradoExcentricidad, 2.0) / 256.0 + 45.0 * Math
								.pow(cuadradoExcentricidad, 3.0) / 1024.0)
						* Math.sin(4.0 * latRadians) - (35.0 * Math.pow(
						cuadradoExcentricidad, 3.0) / 3072.0) * Math.sin(6 * latRadians));
		this.utmEatsing = 0.9996
				* N
				* (A + (1 - T + C) * Math.pow(A, 3.0) / 6 + (5 - 18 * T
						+ Math.pow(T, 2.0) + 72 * C - 58 * cuadradoExcentricidad)
						* Math.pow(A, 5.0) / 120) + 500000;
		this.utmNorting = 0.9996 * (M + N
				* Math.tan(latRadians)
				* (Math.pow(A, 2.0) / 2
						+ (5 - T + 9 * C + 4 * Math.pow(C, 2.0))
						* Math.pow(A, 4.0) / 24 + (61 - 58 * T
						+ Math.pow(T, 2.0) + 600 * C - 330 * cuadradoExcentricidad)
						* Math.pow(A, 6.0) / 720));
	}

	public double getLongitud() {
		return this.longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	public double getLatitud() {
		return this.latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}
	

}
