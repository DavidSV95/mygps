package ModeloGPS;

public class PuntoMapa {
	//private static Double XInicial = 446133.96;
	//private static Double YInicial = 4470973.54;
	private static Double XInicial = 0.0;
	private static Double YInicial = 0.0;
	private static final int LONGY = 1000;
	private static final int LONGX = 1920;
	//private static final double CONSTANTX = 5.03527313734232;
	//private static final double CONSTANTY = 5.199937600748791;
	// pixel/m
	//private static final double CONSTANTX = 4.34699906;
	private static final double CONSTANTX = 4.346982331;
	// m/pixel
	//private static final double CONSTANTXM = 0.230017301;
	private static final double CONSTANTXM = 0.230044643
			;
	/*	private static final Double XInicial = 446661.03;
	private static final Double YInicial = 4471321.08;
	private static final int LONGY = 1000;
	private static final int LONGX = 1920;
	private static final double CONSTANT = 5.7095277746;*/
	private static PuntoMapa puntoMapa = new PuntoMapa();
	private int Ximg;
	private int Yimg;


	public PuntoMapa() {
		Ximg = 0;
		Yimg = 0;
	}

	public static PuntoMapa getPuntoMapa(){
		return puntoMapa;
	}
	public void setXimg(Double x){
		this.Ximg = (int) ((x-XInicial) * CONSTANTX );
		System.out.println("x-->" + x);
		System.out.println("Ximg-->" + Ximg);
	} 
	public void setYimg(Double y){
		this.Yimg = (int) (((YInicial-y) * CONSTANTX));
		System.out.println("y-->" + y);
		System.out.println("Yimg-->" + Yimg);
	} 
	public void ajustarEscalaPuntoMapa(double Xcentro, double Ycentro){
		XInicial = Xcentro - (320*CONSTANTXM);
		System.out.println("XInicial-->" + XInicial);
		YInicial = Ycentro + (320*CONSTANTXM);
		System.out.println("YInicial-->" + YInicial);
		
	}
	
	public int getXimg() {
		// TODO Auto-generated method stub
		return Ximg;
	}

	public int getYimg() {
		// TODO Auto-generated method stub
		return Yimg;
	}

}
