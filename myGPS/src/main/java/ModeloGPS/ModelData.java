package ModeloGPS;

public class ModelData {

	int hora;
	int minuto;
	int segundo;
	double horaGMT;
	double gradosLA;
	double minutosLA;
	double latitud;
	String cardinalidadLatitud;
	double gradosLO;
	double minutosLO;
	double longitud;
	String cardinalidadLongitud;
	int calidadPos;
	int numeroSatelites;
	double dop;
	double altura;
	String unidadMedida;
	double alturaSobreGeoide;
	String unidadMedidaAlturaSobreGeoide;
	double tiempoUltimaModDGPS;
	int idDGPS;
	String crc;

	public ModelData() {
		this.hora = 0;
		this.minuto = 0;
		this.segundo = 0;
		this.horaGMT = 0;
		this.gradosLA = 0.0;
		this.minutosLA = 0.0;
		this.latitud = 0.0;
		this.cardinalidadLatitud = "";
		this.gradosLO = 0.0;
		this.minutosLO = 0.0;
		this.longitud = 0.0;
		this.cardinalidadLongitud = "";
		this.calidadPos = 0;
		this.numeroSatelites = 0;
		this.dop = 0.0;
		this.altura = 0.0;
		this.unidadMedida = "";
		this.alturaSobreGeoide = 0.0;
		this.tiempoUltimaModDGPS = 0.0;
		this.idDGPS = 0;
		this.crc = "";

	}

	public ModelData(String tramaGGA) {
		String data[];
		data = tramaGGA.split(",");
		this.horaGMT = Double.parseDouble(data[1]);

		//this.hora = Integer.parseInt(data[1].substring(0, 2));

		//this.minuto = Integer.parseInt(data[1].substring(2, 4));

		//this.segundo = Integer.parseInt(data[1].substring(4));

		this.gradosLA = Double.parseDouble(data[2].substring(0, 2));
		this.minutosLA = Double.parseDouble(data[2].substring(2));

		this.cardinalidadLatitud = data[3];

		this.gradosLO = Double.parseDouble(data[4].substring(0, 3));
		this.minutosLO = Double.parseDouble(data[4].substring(3));

		this.cardinalidadLongitud = data[5];

		this.calidadPos = Integer.parseInt(data[6]);

		this.numeroSatelites = Integer.parseInt(data[7]);

		this.dop = Double.parseDouble(data[8]);

		this.altura = Double.parseDouble(data[9]);

		this.unidadMedida = data[10];

		this.alturaSobreGeoide = Double.parseDouble(data[11]);
		this.unidadMedidaAlturaSobreGeoide = data[12];
		/*
		 * this.tiempoUltimaModDGPS = Double.parseDouble(data[12]); this.idDGPS
		 * = Integer.parseInt(data[13]);
		 */
		this.crc = data[14];

	}

	public double getLatitud() {

		return this.gradosLA + this.minutosLA / 60.0;

	}

	public double getLongitud() {

		return this.gradosLO + this.minutosLO / 60.0;

	}

}
