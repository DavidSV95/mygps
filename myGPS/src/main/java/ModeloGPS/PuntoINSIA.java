package ModeloGPS;

public class PuntoINSIA {
	private double x;
	private double y;
	private double limiteVelocidad;
	private boolean [] orientacionHoraria = new boolean[2];
	private boolean [] orientacionAntiHoraria = new boolean [2];
	
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public double getLimiteVelocidad() {
		return limiteVelocidad;
	}
	public void setLimiteVelocidad(double limiteVelocidad) {
		this.limiteVelocidad = limiteVelocidad;
	}
	public boolean [] getOrientacionHoraria() {
		return orientacionHoraria;
	}
	public void setOrientacionHoraria(boolean [] orientacionHoraria) {
		this.orientacionHoraria = orientacionHoraria;
	}
	public boolean [] getOrientacionAntiHoraria() {
		return orientacionAntiHoraria;
	}
	public void setOrientacionAntiHoraria(boolean [] orientacionAntiHoraria) {
		this.orientacionAntiHoraria = orientacionAntiHoraria;
	}
	public void pintar() {
		
		System.out.println("X-->" + this.x);
		System.out.println("Y-->"+this.y);
		System.out.println("Limite Velocidad" + this.limiteVelocidad);
		System.out.println("OrientacionHoraria---> (" + this.orientacionHoraria[0]+","+this.orientacionHoraria[1]+")");
		System.out.println("OrientacionAntiHoraria---> (" + this.orientacionAntiHoraria[0]+","+this.orientacionAntiHoraria[1]+")");
		System.out.println("");
		System.out.println("");
		System.out.println("");
	}
	

}
