package ModeloGPS;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.TooManyListenersException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;



import ControladorGPS.Controller;
import ControladorGPS.NewCartesianPointListener;
import javax.swing.JOptionPane;

public class GPS implements SerialPortEventListener {
	private SerialPort puertoSerie;
	private NewCartesianPointListener listener;
	private BufferedReader input;

	public GPS(Controller controller) {
		this.listener = controller;
	}

	public void serialEvent(SerialPortEvent event) {

		if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			new Thread(new Runnable() {
				public void run() {
					String linea = null;
					try {
						if (input.ready()) {
							linea = null;
							linea = input.readLine();
							if (linea != null) {
								parseEphemeridesToGGA(linea);
							}
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}).start();

			// System.out.println(ephemerides);

		}
	}

	public void leer()  {
		try {

			System.loadLibrary("rxtxSerial");
			JOptionPane.showMessageDialog(null, "Se ha cargado la librería nativa correctamente");

		} catch (UnsatisfiedLinkError u) {
			JOptionPane.showMessageDialog(null, "No se ha encontrado la librería nativa de puerto serie");
			
		}

		Enumeration listaPuertos = CommPortIdentifier.getPortIdentifiers();
		CommPortIdentifier idPuerto = null;
		boolean encontrado = false;
		while (listaPuertos.hasMoreElements() && !encontrado) {
			idPuerto = (CommPortIdentifier) listaPuertos.nextElement();
			if (idPuerto.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				if (idPuerto.getName().equals("COM4")) {
					encontrado = true;
				}
			}
		}

		try {
			puertoSerie = (SerialPort) idPuerto.open("GPS App", 1300);
			puertoSerie.setSerialPortParams(115200, SerialPort.DATABITS_8,//115200
					SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
			puertoSerie.notifyOnDataAvailable(true);
			puertoSerie.addEventListener(this);
			this.input = new BufferedReader(new InputStreamReader(
					puertoSerie.getInputStream()));
		}

		catch (PortInUseException e) {
			cerrarPuerto();
			JOptionPane.showMessageDialog(null, "Error abriendo el puerto serie");
		}

		catch (UnsupportedCommOperationException e) {
			cerrarPuerto();
			JOptionPane.showMessageDialog(null, "Error configurando parámetros de configuración");

		}

		catch (TooManyListenersException e) {
			cerrarPuerto();
			e.printStackTrace();
		}
		catch (IOException e){
			cerrarPuerto();
			e.printStackTrace();
		}

	}

	public void cerrarPuerto() {
		JOptionPane.showMessageDialog(null, "Fin de Lectura");
		if (this.puertoSerie != null) {

			try {
				input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				puertoSerie.removeEventListener();
				puertoSerie.close();
			}
		}

	}

	public void parseEphemeridesToGGA(String ephemerides) {
		ModelData data;
		String tramaGGA = "";
		// Pattern pat =
		// Pattern.compile("\\$GPGGA, [\\d]*, [\\d]*.[\\d]*, \\w, [\\d]*.[\\d]*, \\w, [\\d]*, [\\d]*, [\\d]*.[\\d]*, [\\d]*.[\\d]*, \\w, [\\d]*.[\\d]*, [\\d]*.[\\d]*, [\\d]*, \\w*");
		Pattern pat = Pattern.compile("\\$GPGGA,.*$");
		Matcher mat = pat.matcher(ephemerides);

		if (mat.find()) {
			tramaGGA = mat.group(0);
			System.out.println(tramaGGA);
			data = new ModelData(tramaGGA);
			transformToCartesianPoint(data);
		}

	}

	private void transformToCartesianPoint(ModelData data) {
		ModelCartesianPoint cartesianPoint = new ModelCartesianPoint(data);
		listener.newPoint(cartesianPoint);
	}

}
