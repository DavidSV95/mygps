package ControladorGPS;

import ModeloGPS.ModelCartesianPoint;

public interface NewCartesianPointListener {

	public void newPoint(ModelCartesianPoint modelCartesianPoint);
	
}
