package ControladorGPS;

import ModeloGPS.ModelCartesianPoint;
import ModeloGPS.ModelData;
import ModeloGPS.GPS;
import ModeloGPS.PuntoMapa;
import VistasGPS.PanelNavegador;
import VistasGPS.ViewData;
import gnu.io.SerialPortEvent;

import java.awt.Color;
import java.io.IOException;
import java.util.TooManyListenersException;

import javax.swing.JOptionPane;

public class Controller implements NewCartesianPointListener{
	private ViewData view;
	private PanelNavegador panelCasa;
	private GPS miGps;
	private ModelCartesianPoint previousPoint;
	private static final double Frecuencia = 10;
	private boolean sentido; //true = horario, false = antihorario
	private LimiteVelocidad limite;
	private double XimgCentro, YimgCentro;
	private int aux=1;
	
	public void inicializar (){
		miGps = new GPS(this);
		view = new ViewData(miGps);
		try {
			limite = new LimiteVelocidad();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}


	public static void main(String[] args) throws IOException, TooManyListenersException {
		
	
		Controller controller = new Controller();
		controller.inicializar();
		controller.view.setVisible(true);
		
	}


	public void newPoint(ModelCartesianPoint modelCartesianPoint) {
	//	JOptionPane.showMessageDialog(null, "Nuevo punto cartesiano --> UTM_Este: " + modelCartesianPoint.getUtmEatsing() +  " , UTM_Norte: " + modelCartesianPoint.getUtmNorting());
		//String mensaje = "Nuevo punto cartesiano --> UTM_Este: " + modelCartesianPoint.getUtmEatsing() +  " , UTM_Norte: " + modelCartesianPoint.getUtmNorting()+"\n";
		//view.pintarMedida(mensaje);
		
		panelCasa = (PanelNavegador) view.getContentPane();
		
		if(aux==1){
			panelCasa.cargarMapaGoogle(modelCartesianPoint.getLatitud(), modelCartesianPoint.getLongitud());
			XimgCentro = modelCartesianPoint.getUtmEatsing();
			YimgCentro = modelCartesianPoint.getUtmNorting();	
			PuntoMapa.getPuntoMapa().ajustarEscalaPuntoMapa(XimgCentro, YimgCentro);
			PuntoMapa.getPuntoMapa().setXimg(modelCartesianPoint.getUtmEatsing());
			PuntoMapa.getPuntoMapa().setYimg(modelCartesianPoint.getUtmNorting());
			
			aux=0;
		}
		
		final ModelCartesianPoint modelCartesianPointThread=modelCartesianPoint;
		if(PuntoMapa.getPuntoMapa().getXimg()>490 || PuntoMapa.getPuntoMapa().getXimg()<180 || PuntoMapa.getPuntoMapa().getYimg()>490 || PuntoMapa.getPuntoMapa().getYimg()<180){
			new Thread(new Runnable() {
				public void run() {
						panelCasa.cargarMapaGoogle(modelCartesianPointThread.getLatitud(), modelCartesianPointThread.getLongitud());
						XimgCentro = modelCartesianPointThread.getUtmEatsing();
						YimgCentro = modelCartesianPointThread.getUtmNorting();
						PuntoMapa.getPuntoMapa().ajustarEscalaPuntoMapa(XimgCentro, YimgCentro);
					
				}
			}).start();
		}
		
	
		
		PuntoMapa.getPuntoMapa().setXimg(modelCartesianPoint.getUtmEatsing());
		PuntoMapa.getPuntoMapa().setYimg(modelCartesianPoint.getUtmNorting());
		panelCasa.setPuntoMapa(PuntoMapa.getPuntoMapa().getXimg(), PuntoMapa.getPuntoMapa().getYimg());
		//calculamos la velocidad actual.
		double actualSpeed;
		if(previousPoint !=null){
		actualSpeed=  Math.sqrt(Math.pow(
				(modelCartesianPoint.getUtmEatsing() - previousPoint.getUtmEatsing()), 2)
				+ Math.pow((modelCartesianPoint.getUtmNorting() - previousPoint.getUtmNorting()), 2))
				* Frecuencia;
		actualSpeed = actualSpeed * 3.60;
		
		//Calculamos el sentido en funcion de los dos puntos
		int posicionActual = limite.getPosicionMasCercana(modelCartesianPoint); // punto mas cercano al actual
		int posicionAnterior = limite.getPosicionMasCercana(previousPoint);
		if(posicionAnterior<posicionActual){
			sentido = true;
		}
		else if (posicionAnterior>posicionActual) sentido = false;
		
		
		
			double limiteVelocidad = limite.getVelocidadLimite(modelCartesianPoint,previousPoint,sentido);
		//TODO Una vez tenemos la velocidad pintamos la información en el mapa.
			if(actualSpeed < (limiteVelocidad-(limiteVelocidad/10))){
				panelCasa.setColor(Color.GREEN,actualSpeed);
			}
			else if (actualSpeed > (limiteVelocidad-(limiteVelocidad/10)) && actualSpeed < (limiteVelocidad+(limiteVelocidad/10))){
				panelCasa.setColor(Color.YELLOW,actualSpeed);
			}
		
			else if (actualSpeed > (limiteVelocidad+(limiteVelocidad/10))){
				panelCasa.setColor(Color.RED,actualSpeed);
			}
		
		
	}
		
		
		previousPoint = modelCartesianPoint;
	}
	

	
}
