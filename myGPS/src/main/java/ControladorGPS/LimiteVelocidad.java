package ControladorGPS;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import ModeloGPS.ModelCartesianPoint;
import ModeloGPS.PuntoINSIA;




public class LimiteVelocidad {
	private ArrayList<PuntoINSIA> limitPoints;
	private PuntoINSIA puntoSeleccionado; 
	public LimiteVelocidad() throws IOException {
		limitPoints = new ArrayList<PuntoINSIA>();
		FileInputStream fstream = new FileInputStream("C:\\Users\\David\\Desktop\\myGPS\\src\\main\\resources\\Mapa_INSIA2.txt");

		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;

		// Read File Line By Line
		while ((strLine = br.readLine()) != null) {

			String[] array;
			// Print the content on the console
			array = strLine.split("\t");

			PuntoINSIA point = new PuntoINSIA();
			point.setY(Double.parseDouble(array[0]));
			point.setX(Double.parseDouble(array[1]));
			point.setLimiteVelocidad(Float.parseFloat(array[2]));

			limitPoints.add(point);
		}
		for (int i = 0; i<this.limitPoints.size();i++){
			if (i == 0){
				limitPoints.get(i).setOrientacionHoraria(this.getOrientacion(this.limitPoints.get(i),this.limitPoints.get(this.limitPoints.size()-1)));
				limitPoints.get(i).setOrientacionAntiHoraria(this.getOrientacion(this.limitPoints.get(i),this.limitPoints.get(i+1)));
			}
			else if(i==this.limitPoints.size()-1){
				limitPoints.get(i).setOrientacionHoraria(this.getOrientacion(this.limitPoints.get(i),this.limitPoints.get(i-1)));
				limitPoints.get(i).setOrientacionAntiHoraria(this.getOrientacion(this.limitPoints.get(i),this.limitPoints.get(0)));
			}
			else{
				limitPoints.get(i).setOrientacionHoraria(this.getOrientacion(this.limitPoints.get(i),this.limitPoints.get(i-1)));
				limitPoints.get(i).setOrientacionAntiHoraria(this.getOrientacion(this.limitPoints.get(i),this.limitPoints.get(i+1)));
			}
			limitPoints.get(i).pintar();
		}
		
	}

	private boolean[] getOrientacion(PuntoINSIA p1, PuntoINSIA p2) {

			boolean x;
			boolean y;

			if (p1.getX() - p2.getX() > 0) {
				x = true;
			} else {
				x = false;
			}

			if (p1.getY() - p2.getY() > 0) {
				y = true;
			} else {
				y = false;
			}

			boolean[]  or = new boolean[]{x,y};
			return or;
		}
	



	public double getVelocidadLimite(ModelCartesianPoint actualPoint, ModelCartesianPoint previousPoint,
			boolean sentido) {
		int contador = 0;
		double limiteProvisional;
		double menorDistancia = 25000;
		int posicion = 0;

		for (PuntoINSIA point : limitPoints) {
			if (sentido){
			// Comparo la distancia
			// Si es menor que la distancia que tengo guardada y la orientacion
			// coincide,la actualizo
			if (distancia(actualPoint, point) < menorDistancia
					&& sameOrientation(actualPoint,previousPoint,point.getOrientacionHoraria() )) {
				menorDistancia = distancia(actualPoint, point);
				puntoSeleccionado = point;

				posicion = limitPoints.indexOf(point);
			}
			contador++;
		}
			else {
				// Comparo la distancia
				// Si es menor que la distancia que tengo guardada y la orientacion
				// coincide,la actualizo
				if (distancia(actualPoint, point) < menorDistancia
						&& sameOrientation(actualPoint,previousPoint,point.getOrientacionAntiHoraria() )) {
					menorDistancia = distancia(actualPoint, point);
					puntoSeleccionado = point;

					posicion = limitPoints.indexOf(point);
				}
				contador++;
			}
		}
		PuntoINSIA puntoSiguiente = limitPoints.get(posicion + 1 % contador);
		PuntoINSIA puntoAnterior = limitPoints.get(posicion - 1 % contador);
		if (distancia(actualPoint, puntoSiguiente) < distancia(actualPoint,
				puntoAnterior)) {
			// Estoy entre el siguiente y seleccionado
			limiteProvisional = puntoSeleccionado.getLimiteVelocidad();
		} else {
			// Estoy entre el anterior y el seleccionado
			limiteProvisional = puntoAnterior.getLimiteVelocidad();
		}

		// Si la distancia con el punto obtenido es menor de 5 metros
		// el punto es correcto y devolvemos la velocidad del punto
		if (menorDistancia < 5) {
			return limiteProvisional;
		}
		// Sino, devolvemos -1.
		else {
			return -1;
		}

	}

	private boolean sameOrientation(ModelCartesianPoint actualPoint, ModelCartesianPoint previousPoint,
			boolean[] orientacionHoraria) {
		boolean [] aux = this.orientacion(actualPoint, previousPoint);
			if(aux[0]==orientacionHoraria[0] && aux[1]==orientacionHoraria[1]) return true;
			else return false;
		
	}

	private boolean[] orientacion(ModelCartesianPoint p1, ModelCartesianPoint p2) {
		boolean x;
		boolean y;

		if (p1.getUtmEatsing() - p2.getUtmEatsing() > 0) {
			x = true;
		} else {
			x = false;
		}

		if (p1.getUtmNorting() - p2.getUtmNorting() > 0) {
			y = true;
		} else {
			y = false;
		}

		boolean[]  or = new boolean[]{x,y};
		return or;
	}

	public int getPosicionMasCercana(ModelCartesianPoint point) {
		float menorDistancia = 99999;
		int pos = -1;
		float distanciaAux;
		for (int i = 0; i < this.limitPoints.size(); i++) {
			distanciaAux = distancia(point, limitPoints.get(i));
			if (i == 0) {
				menorDistancia = distanciaAux;
				pos = i;
			} else {
				if (menorDistancia > distanciaAux) {
					menorDistancia = distanciaAux;
					pos = i;
				}
			}
		}
		return pos;
	}

	public float distancia(ModelCartesianPoint p1, PuntoINSIA point) {
		return (float) Math.sqrt(
				Math.pow((p1.getUtmEatsing() - point.getX()), 2) + Math.pow((p1.getUtmNorting() - point.getY()), 2));
	}

}
