# README #

ATENCIÓN: No se puede probar la aplicación sin un receptor GPS.

El proyecto myGPS fue realizado en la asignatura Sistemas Y Servicios de Navegación Mediante GPS por mí y por 2 compañeros más de clase, aunque el desarrollo y entrega de la práctica final fue individual. En dicha asignatura mi calificación fue un 9.

La aplicación tiene 2 funcionalidades, la primera que es leer los datos recibidos del GPS y conseguir las coordenadas (“Iniciar conexión GPS” en la aplicación), y la segunda es representar la posición real en una imagen proporcionada por google maps y medir la velocidad a la que nos movemos indicando si excedemos la velocidad permitida (“Comenzar Navegación” en la aplicación).

Las prácticas de esta asignatura las realizamos en un coche eléctrico en el Instituto Universitario de Investigación del Automóvil (INSIA)), donde previamente tenemos asignados unos límites de velocidad en el circuito, en los que la aplicación te avisa si los superas.
También adjunto la documentación de la práctica realizada.